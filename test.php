<?php

include './vendor/autoload.php';

use Zolli\PrometheusPHP\Collection\MetricsCollection;
use Zolli\PrometheusPHP\Factory\MetricsFactory;
use Zolli\PrometheusPHP\Label\Label;
use Zolli\PrometheusPHP\Metadata\MetadataType;
use Zolli\PrometheusPHP\Metrics\Type\Gauge;
use Zolli\PrometheusPHP\Serializer\StandardSerializer;
use Zolli\PrometheusPHP\Storage\InMemoryStorage;

$factory = new MetricsFactory();
$storage = new InMemoryStorage();
$collection = new MetricsCollection($factory, $storage);

$collection->add(function(MetricsFactory $factory) {
    $rootMetrics = $factory->create(Gauge::class);
    $metadata = $rootMetrics->getMetadataStore();
    $metadata->set(MetadataType::HELP, 'help message');

    $rootMetrics
        ->setName('network_bytes_out')
        ->addLabel(new Label('device', 'ens_160'))
        ->setValue('1024');

    $rootMetrics->createVariant(function(Gauge $metrics) {
        return $metrics
            ->addLabel(new Label('device', 'ens_192'))
            ->setValue('10240');
    });

    return $rootMetrics;
});

$collection->add(function(MetricsFactory $factory) {
    $rootMetrics = $factory->create(Gauge::class);
    $rootMetrics->setName('asd')->setValue('asder')->addLabel(new Label('asd', 'asdvalue'));

    return $rootMetrics;
});

$serializer = new StandardSerializer();

echo $serializer->serialize($collection);
