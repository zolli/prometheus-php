<?php

namespace Zolli\PrometheusPHP\Tests\Unit\Storage;

use PHPUnit\Framework\TestCase;
use Zolli\PrometheusPHP\Exception\MetricsAlreadyStored;
use Zolli\PrometheusPHP\Storage\InMemoryStorage;
use Zolli\PrometheusPHP\Tests\Fixture\Metrics\DummyMetrics;
use Zolli\PrometheusPHP\Tests\Utils\ReflectionUtils;

/**
 *
 */
class InMemoryStorageTest extends TestCase
{

    use ReflectionUtils;

    public function testTheInitialStateIsAnEmptyArray()
    {
        $storage = new InMemoryStorage();
        $storageContent = $this->getObjectProperty($storage, 'data');

        $this->assertIsArray($storageContent);
        $this->assertEquals([], $storageContent);
    }

    public function testItCanStoreObjects()
    {
        $storage = new InMemoryStorage();
        $dummyMetrics = new DummyMetrics();
        $dummyMetricsHash = spl_object_hash($dummyMetrics);

        $storage->store($dummyMetrics);

        $storageContent = $this->getObjectProperty($storage, 'data');

        $this->assertIsArray($storageContent);
        $this->assertArrayHasKey($dummyMetricsHash, $storageContent);
        $this->assertCount(1, $storageContent);
    }

    public function testItThrowsExceptionWhenTryingToAddMetricsThatAlreadyStored()
    {
        $storage = new InMemoryStorage();
        $dummyMetrics = new DummyMetrics();

        $storage->store($dummyMetrics);

        $this->expectException(MetricsAlreadyStored::class);
        $this->expectExceptionMessage('This metrics is already stored!');

        $storage->store($dummyMetrics);
    }

    public function testItCanReturnsAnEmptyArrayIfNoMetricsAreStored()
    {
        $storage = new InMemoryStorage();
        $result = $storage->getAll();

        $this->assertIsArray($result);
        $this->assertCount(0, $result);
    }

}
