<?php

namespace Zolli\PrometheusPHP\Tests\Unit\Factory;

use PHPUnit\Framework\TestCase;
use Zolli\PrometheusPHP\Exception\CannotDeclareFactory;
use Zolli\PrometheusPHP\Factory\MetricsFactory;
use Zolli\PrometheusPHP\Metrics\Type\Gauge;
use Zolli\PrometheusPHP\Tests\Utils\ReflectionUtils;

/**
 * @author Zoltán Borsos <zolli07@gmail.com>
 */
class MetricsFactoryTest extends TestCase
{

    use ReflectionUtils;

    public function factoryProviderDeterminationDataProvider(): array
    {
        return [
            [Gauge::class, true],
            [\DateTime::class, false],
        ];
    }

    public function notFactoryProviderImplProvider(): array
    {
        return [
            [\DateTime::class],
            ['test'],
        ];
    }

    /**
     * @param string $class
     * @param bool $expected
     *
     * @throws \ReflectionException
     *
     * @dataProvider factoryProviderDeterminationDataProvider
     */
    public function testItCanDetermineFactoryProviderImplementation(string $class, bool $expected)
    {
        $factory = new MetricsFactory();
        $result = $this->callMethod($factory, 'classIsFactoryProvider', [$class]);

        $this->assertEquals($expected, $result);
    }

    /**
     * @param string $class
     *
     * @throws CannotDeclareFactory
     *
     * @dataProvider notFactoryProviderImplProvider
     */
    public function testItDetectsFalseInputsProperly(string $class)
    {
        $this->expectExceptionMessage('This class cannot declare factory method, or not exist!');
        $this->expectException(CannotDeclareFactory::class);

        $factory = new MetricsFactory();
        $factory->create($class);
    }

    public function testItCanCreatesMetricsUsingTheFactory()
    {
        $factory = new MetricsFactory();
        $result = $factory->create(Gauge::class);

        $this->assertIsObject($result);
        $this->assertInstanceOf(Gauge::class, $result);
    }

}
