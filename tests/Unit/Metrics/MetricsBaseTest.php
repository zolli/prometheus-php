<?php

namespace Zolli\PrometheusPHP\Tests\Unit\Metrics;

use PHPUnit\Framework\TestCase;
use Zolli\PrometheusPHP\Exception\ForbiddenNameOverriding;
use Zolli\PrometheusPHP\Exception\NonRootElementException;
use Zolli\PrometheusPHP\Label\Label;
use Zolli\PrometheusPHP\Tests\Fixture\Metrics\MetricsBaseImpl;
use Zolli\PrometheusPHP\Tests\Utils\ReflectionUtils;

/**
 * MetricsBase unit tests
 *
 * @author Zoltán Borsos <zolli07@gmail.com>
 */
class MetricsBaseTest extends TestCase
{

    use ReflectionUtils;

    public function testItThrowsExceptionWhenTryToSetNameFieldInAVariant()
    {
        $metrics = new MetricsBaseImpl();

        $returnedInstance = $metrics->setName('dummy');

        $this->assertEquals($metrics, $returnedInstance);
        $this->assertIsObject($returnedInstance);
        $this->assertEquals('dummy', $metrics->getName());

        $this->setPropertyValue($metrics, 'isVariant', true, 1);

        $this->expectException(ForbiddenNameOverriding::class);
        $this->expectExceptionMessage('You cannot override name of a metrics variant! The name is set in the root metrics.');

        $metrics->setName('dummy2');
    }

    public function testVariantCreationWorkingProperly()
    {
        $metrics = new MetricsBaseImpl();

        $metrics
            ->setName('name')
            ->setValue('value')
            ->addLabel(new Label('label', 'value'));

        $metrics->createVariant(function(MetricsBaseImpl $metricsVariant) use ($metrics) {
             $this->assertNotSame($metricsVariant, $metrics);
             $this->assertEmpty($metricsVariant->getLabels());
             $this->assertNull($metricsVariant->getValue());
             $this->assertIsArray($this->getObjectProperty($metricsVariant, 'variants', 1));
             $this->assertTrue($this->getObjectProperty($metricsVariant, 'isVariant', 1));

             $this->expectException(NonRootElementException::class);
             $this->expectExceptionMessage('This metrics not able to create a variant, because it is already a variant!');

             $metricsVariant->createVariant(function(MetricsBaseImpl $test) {});

             $this->expectException(NonRootElementException::class);
             $this->expectExceptionMessage('This is a non-root metrics, that cannot be able to store any variants!');

             $metricsVariant->getVariants();

             $this->expectException(ForbiddenNameOverriding::class);
             $this->expectExceptionMessage('You cannot override name of a metrics variant! The name is set in the root metrics.');

             $metricsVariant->setName('new_name');

             return $metrics;
        });
    }

}
