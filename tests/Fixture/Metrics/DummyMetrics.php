<?php

namespace Zolli\PrometheusPHP\Tests\Fixture\Metrics;

use Doctrine\Common\Collections\Collection;
use Zolli\PrometheusPHP\Label\Label;
use Zolli\PrometheusPHP\Metrics\Metrics;

/**
 *
 */
class DummyMetrics implements Metrics
{

    /**
     * @inheritDoc
     */
    public function getName(): ?string
    {
        // TODO: Implement getName() method.
    }

    /**
     * @inheritDoc
     */
    public function setName(string $name): Metrics
    {
        // TODO: Implement setName() method.
    }

    /**
     * @inheritDoc
     */
    public function getLabels(): Collection
    {
        // TODO: Implement getLabels() method.
    }

    /**
     * @inheritDoc
     */
    public function addLabel(Label $label): Metrics
    {
        // TODO: Implement setLabels() method.
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        // TODO: Implement getType() method.
    }

    /**
     * @inheritDoc
     */
    public function getValue()
    {
        // TODO: Implement getValue() method.
    }

    /**
     * @inheritDoc
     */
    public function setValue($value): Metrics
    {
        // TODO: Implement setValue() method.
    }

    /**
     * @inheritDoc
     */
    public function createVariant(callable $callback): Metrics
    {
        // TODO: Implement createVariant() method.
    }

    /**
     * @inheritDoc
     */
    public function getVariants(): array
    {
        // TODO: Implement getVariants() method.
    }

}
