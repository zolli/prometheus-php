<?php

namespace Zolli\PrometheusPHP\Tests\Fixture\Metrics;

use Zolli\PrometheusPHP\Metrics\MetricsBase;

/**
 * Dummy MetricsBase implementation for tests
 *
 * @author Zoltán Borsos <zolli07@gmail.com>
 */
class MetricsBaseImpl extends MetricsBase
{
    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return 'metrics_base_impl';
    }

}
