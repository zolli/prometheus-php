<?php

namespace Zolli\PrometheusPHP\Tests\Utils;

/**
 *
 */
trait ReflectionUtils
{

    /**
     * Returns the value of an object property, even if is not in public visibility
     *
     * @param object $object
     * @param string $propertyName
     *
     * @param int $depth
     *
     * @return mixed
     */
    public function getObjectProperty(object $object, string $propertyName, int $depth = 0)
    {
        return $this->getPropertyReflector($object, $propertyName, $depth)->getValue($object);
    }

    public function setPropertyValue(object $object, string $propertyName, string $propertyValue, int $depth = 0)
    {
        $this->getPropertyReflector($object, $propertyName, $depth)->setValue($object, $propertyValue);
    }

    protected function getPropertyReflector(object $object, string $propertyName, int $depth = 0): \ReflectionProperty
    {
        /** @var \ReflectionObject $objectReflector */
        $objectReflector = new \ReflectionObject($object);

        if ($depth > 0) {
            $i = 0;

            while ($i < $depth) {
                $objectReflector = $objectReflector->getParentClass();

                $i++;
            }
        }

        if ($objectReflector->hasProperty($propertyName)) {
            /** @var \ReflectionProperty $propertyReflector */
            $propertyReflector = $objectReflector->getProperty($propertyName);

            if ($propertyReflector->isPrivate() || $propertyReflector->isProtected()) {
                $propertyReflector->setAccessible(true);
            }

            return $propertyReflector;
        }

        throw new \RuntimeException(sprintf("The object cannot contains the %s property!", $propertyName));
    }

    /**
     * Executes a method on an object, even if is not in public visibility level
     *
     * @param object $object
     * @param string $methodName
     * @param array $arguments
     *
     * @return mixed
     *
     * @throws \ReflectionException
     */
    public function callMethod(object $object, string $methodName, array $arguments = [])
    {
        /** @var \ReflectionObject $objectReflector */
        $objectReflector = new \ReflectionObject($object);

        if ($objectReflector->hasMethod($methodName)) {
            /** @var \ReflectionMethod $methodReflector */
            $methodReflector = $objectReflector->getMethod($methodName);

            if ($methodReflector->isPrivate() || $methodReflector->isProtected())
            {
                $methodReflector->setAccessible(true);
            }

            return $methodReflector->invokeArgs($object, $arguments);
        }

        throw new \LogicException(sprintf('This object cannot define %s method!', $methodName));
    }

}
