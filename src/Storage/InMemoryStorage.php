<?php

namespace Zolli\PrometheusPHP\Storage;

use Zolli\PrometheusPHP\Exception\MetricsAlreadyStored;
use Zolli\PrometheusPHP\Metrics\Metrics;

/**
 * In-Memory metrics storage implementation
 *
 * @author Zoltán Borsos <zolli07@gmail.com>
 */
class InMemoryStorage implements MetricsStorage
{

    private $data = [];

    /**
     * @inheritdoc
     *
     * @throws MetricsAlreadyStored
     */
    public function store(Metrics $metrics): void
    {
        $hash = spl_object_hash($metrics);

        if (isset($this->data[$hash])) {
            throw new MetricsAlreadyStored('This metrics is already stored!');
        }

        $this->data[$hash] = $metrics;
    }

    /**
     * @inheritdoc
     */
    public function getAll(): array
    {
        return $this->data;
    }


}
