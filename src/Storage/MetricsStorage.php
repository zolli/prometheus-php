<?php

namespace Zolli\PrometheusPHP\Storage;

use Zolli\PrometheusPHP\Metrics\Metrics;

/**
 * This interface defines common behaviour of storing metrics
 * in different storage engines
 *
 * @author Zoltán Borsos <zolli07@gmail.com>
 */
interface MetricsStorage
{

    /**
     * Stores the metrics in the storage engine
     *
     * @param Metrics $metrics
     */
    public function store(Metrics $metrics): void;

    /**
     * returns all metrics currently stored in the storage engine
     *
     * @return array
     */
    public function getAll(): array;

}
