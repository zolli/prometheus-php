<?php

namespace Zolli\PrometheusPHP\Serializer;

use Zolli\PrometheusPHP\Collection\MetricsCollection;
use Zolli\PrometheusPHP\Label\Label;
use Zolli\PrometheusPHP\Metadata\MetadataType;
use Zolli\PrometheusPHP\Metrics\MetadataProvider;
use Zolli\PrometheusPHP\Metrics\Metrics;

/**
 *
 */
class StandardSerializer
{

    const HELP_TEMPLATE = '# HELP %s %s';

    const TYPE_HEADER = '# TYPE %s %s';

    /**
     * @var array
     */
    private $outputBuffer = [];

    public function serialize(MetricsCollection $collection): string
    {
        /** @var Metrics $singleMetrics */
        foreach ($collection->getIterator() as $singleMetrics) {
            $this->generateFromSingleMetrics($singleMetrics);
        }

        return implode("\r\n", $this->outputBuffer);
    }

    private function generateFromSingleMetrics(Metrics $metrics)
    {
        $this->generateHelpMessage($metrics);
        $this->generateTypeHeader($metrics);
        $this->generateMetricsValues($metrics);

        if (!empty($metrics->getVariants())) {
            foreach($metrics->getVariants() as $variant) {
                $this->generateMetricsValues($variant);
            }
        }
    }

    private function generateHelpMessage(Metrics $metrics)
    {
        if ($metrics instanceof MetadataProvider) {
            $metadataStore = $metrics->getMetadataStore();

            if ($metadataStore->has(MetadataType::HELP)) {
                $this->outputBuffer[] = sprintf(
                    self::HELP_TEMPLATE,
                    $metrics->getName(),
                    $metadataStore->get(MetadataType::HELP)
                );
            }
        }
    }

    private function generateTypeHeader(Metrics $metrics)
    {
        $this->outputBuffer[] = sprintf(
            self::TYPE_HEADER,
            $metrics->getName(),
            $metrics->getType()
        );
    }

    private function generateMetricsValues(Metrics $metrics)
    {
        $labels = $metrics->getLabels();
        $labelStr = '';

        if (!$labels->isEmpty()) {
            $rawFormattedLabels = [];

            /** @var Label $label */
            foreach ($labels as $label) {
                $rawFormattedLabels[] = $label->getFormatted();
            }

            $labelStr = implode(',', $rawFormattedLabels);
            $labelStr = sprintf('{%s}', $labelStr);
        }

        $this->outputBuffer[] = $metrics->getName() . $labelStr . ' ' . $metrics->getValue();

    }

}
