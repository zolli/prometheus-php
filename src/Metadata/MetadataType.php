<?php

namespace Zolli\PrometheusPHP\Metadata;

/**
 * Metadata type enum
 *
 * @author Zoltán Borsos <zolli07@gmail.com>
 */
final class MetadataType
{

    const HELP = 'help';

}
