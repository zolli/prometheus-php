<?php

namespace Zolli\PrometheusPHP\Metadata;

/**
 * Simple metadata store
 *
 * @author Zoltán Borsos <zolli07@gmail.com>
 */
class MetadataStore
{

    /**
     * @var array
     */
    private $data = [];

    public function set(string $type, $value): void
    {
        $this->data[$type] = $value;
    }

    public function get(string $type)
    {
        return isset($this->data[$type]) ? $this->data[$type] : null;
    }

    public function has(string $type): bool
    {
        return isset($this->data[$type]);
    }

}
