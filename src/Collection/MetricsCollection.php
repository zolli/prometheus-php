<?php

namespace Zolli\PrometheusPHP\Collection;

use Zolli\PrometheusPHP\Collection\Iterator\MetricsCollectionIterator;
use Zolli\PrometheusPHP\Factory\MetricsFactory;
use Zolli\PrometheusPHP\Storage\MetricsStorage;

/**
 * @author Zoltán Borsos <zolli07@gmail.com>
 */
class MetricsCollection
{

    /**
     * @var MetricsFactory
     */
    private $metricsFactory;

    /**
     * @var MetricsStorage
     */
    private $metricsStorage;

    public function __construct(MetricsFactory $factory, MetricsStorage $storage)
    {
        $this->metricsFactory = $factory;
        $this->metricsStorage = $storage;
    }

    public function add(callable $callback): self
    {
        /** @var Metrics|null $metrics */
        $metrics = call_user_func_array($callback, [$this->metricsFactory]);

        if (!$metrics instanceof Metrics) {
            //TODO: throw exception
        }

        $this->metricsStorage->store($metrics);

        return $this;
    }

    public function getAll()
    {
        return $this->metricsStorage->getAll();
    }

    public function getIterator()
    {
        return new MetricsCollectionIterator($this);
    }

}
