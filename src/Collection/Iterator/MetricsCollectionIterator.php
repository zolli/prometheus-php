<?php

namespace Zolli\PrometheusPHP\Collection\Iterator;

use Zolli\PrometheusPHP\Collection\MetricsCollection;
use Zolli\PrometheusPHP\Metrics\Metrics;

/**
 *
 */
class MetricsCollectionIterator implements \ArrayAccess, \Iterator
{

    /**
     * @var Metrics[]
     */
    private $data;

    /**
     * @inheritDoc
     */
    public function __construct(MetricsCollection $collection)
    {
        $this->data = $collection->getAll();
    }

    /**
     * @inheritDoc
     */
    public function offsetExists($offset)
    {
        return isset($this->data[$offset]);
    }

    /**
     * @inheritDoc
     */
    public function offsetGet($offset)
    {
        if ($this->offsetExists($offset)) {
            return $this->data[$offset];
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function offsetSet($offset, $value)
    {
        throw new \RuntimeException('This is an immutable collection!');
    }

    /**
     * @inheritDoc
     */
    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }

    /**
     * @inheritDoc
     */
    public function current()
    {
        return current($this->data);
    }

    /**
     * @inheritDoc
     */
    public function next()
    {
        return next($this->data);
    }

    /**
     * @inheritDoc
     */
    public function key()
    {
        return key($this->data);
    }

    /**
     * @inheritDoc
     */
    public function valid()
    {
        $key = $this->key();

        return ($key !== null && $key !== false);
    }

    /**
     * @inheritDoc
     */
    public function rewind()
    {
        reset($this->data);
    }

}
