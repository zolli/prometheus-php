<?php

namespace Zolli\PrometheusPHP\Factory;

use Zolli\PrometheusPHP\Exception\CannotDeclareFactory;
use Zolli\PrometheusPHP\Metrics\FactoryProvider;
use Zolli\PrometheusPHP\Metrics\Metrics;

/**
 * Metrics factory
 *
 * @author Zoltán Borsos <zolli07@gmail.com>
 */
class MetricsFactory
{

    /**
     * Creates a metrics using its factory method
     *
     * @param string $className
     *
     * @return Metrics
     *
     * @throws CannotDeclareFactory
     */
    public function create(string $className):  Metrics
    {
        if (!class_exists($className) ||!$this->classIsFactoryProvider($className)) {
            throw new CannotDeclareFactory('This class cannot declare factory method, or not exist!');
        }

        return call_user_func([$className, FactoryProvider::FACTORY_METHOD_NAME]);
    }

    /**
     * Determines if the given class can provide a factory method
     *
     * @param string $class
     *
     * @return bool
     */
    private function classIsFactoryProvider(string $class): bool
    {
        return in_array(FactoryProvider::class, class_implements($class));
    }

}
