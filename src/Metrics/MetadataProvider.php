<?php

namespace Zolli\PrometheusPHP\Metrics;

use Zolli\PrometheusPHP\Metadata\MetadataStore;

/**
 * This interface defines metrics that expose a 'metadataStore', for registering
 * additional data to the metrics.
 *
 * In example, the help message is an optional metadata for the metrics, its
 * not required by prometheus
 *
 * @author Zoltán Borsos <zolli07@gmail.com>
 */
interface MetadataProvider
{

    /**
     * Returns the metadata store
     *
     * @return MetadataStore
     */
    public function getMetadataStore(): MetadataStore;

}
