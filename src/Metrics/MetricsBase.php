<?php

namespace Zolli\PrometheusPHP\Metrics;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Zolli\PrometheusPHP\Exception\ForbiddenNameOverriding;
use Zolli\PrometheusPHP\Exception\NonRootElementException;
use Zolli\PrometheusPHP\Label\Label;

/**
 * Base implementation of the metrics interface
 *
 * @author Zoltán Borsos <zolli07@gmail.com>
 */
abstract class MetricsBase implements Metrics
{

    /**
     * @var Collection
     */
    private $labels;

    /**
     * @var mixed
     */
    private $value;

    /**
     * @var string
     */
    private $name;

    /**
     * @var bool
     */
    private $isVariant = false;

    /**
     * @var Metrics[]
     */
    private $variants = [];

    /**
     * @inheritDoc
     */
    public function __construct()
    {
        $this->labels = new ArrayCollection();
    }

    /**
     * @inheritDoc
     *
     * @codeCoverageIgnore
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     *
     * @throws ForbiddenNameOverriding
     */
    public function setName(string $name): Metrics
    {
        if ($this->isVariant) {
            throw new ForbiddenNameOverriding(
                'You cannot override name of a metrics variant! The name is set in the root metrics.'
            );
        }

        $this->name = $name;

        return $this;
    }


    /**
     * @inheritDoc
     *
     * @codeCoverageIgnore
     */
    public function getLabels(): Collection
    {
        return $this->labels;
    }

    /**
     * @inheritDoc
     *
     * @codeCoverageIgnore
     */
    public function addLabel(Label $label): Metrics
    {
        $this->labels->add($label);

        return $this;
    }

    /**
     * @inheritDoc
     *
     * @codeCoverageIgnore
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @inheritDoc
     *
     * @codeCoverageIgnore
     */
    public function setValue($value): Metrics
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @inheritdoc
     *
     * @throws NonRootElementException
     */
    public function createVariant(callable $callback): Metrics
    {
        if ($this->isVariant) {
            throw new NonRootElementException(
                'This metrics not able to create a variant, because it is already a variant!'
            );
        }

        $clone = clone $this;
        $clone->isVariant = true;
        $clone->variants = [];
        $clone->value = null;
        $clone->labels = new ArrayCollection();

        $this->variants[] = call_user_func_array($callback, [$clone]);

        return $this;
    }

    /**
     * @inheritdoc
     *
     * @throws NonRootElementException
     */
    public function getVariants(): array
    {
        if ($this->isVariant) {
            throw new NonRootElementException(
                'This is a non-root metrics, that cannot be able to store any variants!'
            );
        }

        return $this->variants;
    }

}
