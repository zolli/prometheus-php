<?php

namespace Zolli\PrometheusPHP\Metrics;

use Doctrine\Common\Collections\Collection;
use Zolli\PrometheusPHP\Label\Label;

/**
 * Metrics interface
 *
 * @author Zoltán Borsos <zolli07@gmail.com>
 */
interface Metrics
{

    /**
     * Returns the metrics name
     *
     * @return string
     */
    public function getName(): ?string;


    /**
     * Sets the metrics name
     *
     * @param string $name
     *
     * @return Metrics
     */
    public function setName(string $name): Metrics;

    /**
     * Returns the label of the metrics
     *
     * @return Collection
     */
    public function getLabels(): Collection;

    /**
     * Sets the metrics label
     *
     * @param Label $label
     *
     * @return Metrics
     */
    public function addLabel(Label $label): Metrics;

    /**
     * Get the label value
     *
     * @return mixed
     */
    public function getValue();

    /**
     * Sets the metrics value
     *
     * @param mixed $value
     *
     * @return Metrics
     */
    public function setValue($value): Metrics;

    /**
     * Creates a new variation of this exact metrics. This will removes
     * the label and the value and leave the name untouched
     *
     * @param callable $callback
     *
     * @return self
     */
    public function createVariant(callable $callback): self;

    /**
     * Return the variants of this metrics, if this is a single-metrics
     * the result of this function will be an empty array
     *
     * @return Metrics[]|array
     */
    public function getVariants(): array;

    /**
     * Returns the string representation of this metrics.
     * This mainly used during serialization, to generate the type header
     *
     * @return string
     */
    public function getType(): string;

}
