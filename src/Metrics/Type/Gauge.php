<?php

namespace Zolli\PrometheusPHP\Metrics\Type;

use Zolli\PrometheusPHP\Metadata\MetadataStore;
use Zolli\PrometheusPHP\Metrics\{
    FactoryProvider,
    MetadataProvider,
    MetricsBase
};

/**
 * Gauge type metrics
 *
 * @author Zoltán Borsos <zolli07@gmail.com>
 */
class Gauge extends MetricsBase implements FactoryProvider, MetadataProvider
{

    /**
     * @var MetadataStore
     */
    private $metadataStore;

    /**
     * @inheritDoc
     */
    public function __construct()
    {
        $this->metadataStore = new MetadataStore();

        parent::__construct();
    }


    public static function create()
    {
        return new self();
    }

    public function getMetadataStore(): MetadataStore
    {
        return $this->metadataStore;
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return 'gauge';
    }

}
