<?php

namespace Zolli\PrometheusPHP\Metrics;

/**
 * This interface define classes that exposes a factory method to use with
 * the MetricsFactory class
 */
interface FactoryProvider
{

    const FACTORY_METHOD_NAME = 'create';

    /**
     * Creates a new instance from this metrics
     *
     * @return Metrics
     */
    public static function create();

}
