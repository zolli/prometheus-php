<?php

namespace Zolli\PrometheusPHP\Exception;

use \Exception;

/**
 * ForbiddenNameOverriding exception
 *
 * @author Zoltán Borosos <zolli07@gmail.com>
 */
class ForbiddenNameOverriding extends Exception
{

}
