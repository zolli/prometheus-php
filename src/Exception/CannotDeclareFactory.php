<?php

namespace Zolli\PrometheusPHP\Exception;

use \Exception;

/**
 * CannotDeclareFactory
 *
 * @author Zoltán Borsos <zolli07@gmail.com>
 */
class CannotDeclareFactory extends Exception
{

}
