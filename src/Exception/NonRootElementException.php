<?php

namespace Zolli\PrometheusPHP\Exception;

use \Exception;

/**
 * Non root element exception
 *
 * @author Zoltán Borsos <zolli07@gmail.com>
 */
class NonRootElementException extends Exception
{

}
